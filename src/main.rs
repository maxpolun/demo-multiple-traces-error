use actix_web::{Error, dev::{Service, ServiceRequest, ServiceResponse, Transform}, middleware::Logger};
use actix_web::{web, App, HttpServer};
use actix_web_opentelemetry::RequestTracing;
use opentelemetry::{global, sdk::propagation::TraceContextPropagator};
use tracing::{Instrument, Level, info, span};
use std::{cell::RefCell, future::{Future, Ready, ready}, io, pin::Pin, rc::Rc, task::{Context, Poll}};
use tracing_subscriber::prelude::*;
use tracing_subscriber::Registry;

async fn index(username: actix_web::web::Path<String>) -> String {
    greet_user(username.as_ref())
}

#[tracing::instrument]
fn greet_user(username: &str) -> String {
    tracing::info!("preparing to greet user");
    format!("Hello {}", username)
}


pub struct MiddlewareWrapper;
pub struct MiddlewareService<S: Service> {
    service: Rc<RefCell<S>>
  }


impl<S, B> Transform<S> for MiddlewareWrapper
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>
        + 'static,
    S::Future: 'static + std::future::Future,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = MiddlewareService<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(MiddlewareService {
            service: Rc::new(RefCell::new(service)),
        }))
    }
}


impl<S, B> Service for MiddlewareService<S>
where
    B: 'static,
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>
        + 'static,
    S::Future: 'static + std::future::Future,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&mut self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.borrow_mut().poll_ready(ctx)
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        let span = span!(Level::INFO, "Middleware");
        let srv = self.service.clone();

        Box::pin(async move {
          info!("In the middleware");
          let _ = ready("do some async work").await;
          Ok(srv.borrow_mut().call(req).await?)

        }.instrument(span))
    }
}

#[actix_web::main]
async fn main() -> io::Result<()> {

    // Start an otel jaeger trace pipeline
    global::set_text_map_propagator(TraceContextPropagator::new());
    let (tracer, _uninstall) = opentelemetry_jaeger::new_pipeline()
        .with_service_name("app_name")
        .install()
        .unwrap();

    // Initialize `tracing` using `opentelemetry-tracing` and configure logging
    Registry::default()
        .with(tracing_subscriber::EnvFilter::new("INFO"))
        .with(tracing_subscriber::fmt::layer())
        .with(tracing_opentelemetry::layer().with_tracer(tracer))
        .init();

    // Start actix web with otel and tracing middlewares
    HttpServer::new(move || {
        App::new()
            .wrap(MiddlewareWrapper)
            .wrap(Logger::default())
            .wrap(RequestTracing::new())
            .service(web::resource("/users/{username}").to(index))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
